<?php

chdir(__DIR__);
require_once '../vendor/php-test-framework/public-api.php';

const BASE_URL = 'http://localhost:8080/ex1/';

setBaseUrl(BASE_URL);

test('index to a', function () {
    navigateTo(BASE_URL);

    clickLinkWithText("a.html");

    assertCurrentUrl(BASE_URL . "a/a.html");
});

test('a to e', function () {
    navigateTo(BASE_URL . 'a/a.html');

    clickLinkWithText("e.html");

    assertCurrentUrl(BASE_URL . "a/b/c/d/e/e.html");
});

test('e to d', function () {
    navigateTo(BASE_URL . 'a/b/c/d/e/e.html');

    clickLinkWithText("d.html");

    assertCurrentUrl(BASE_URL . "a/b/c/d/d.html");
});

test('d to b', function () {
    navigateTo(BASE_URL . 'a/b/c/d/d.html');

    clickLinkWithText("b.html");

    assertCurrentUrl(BASE_URL . "a/b/b.html");
});

test('Shortest self', function () {
    navigateTo(BASE_URL . 'a/b/c/d/e/f/f.html');

    $linkText = "shortest self";

    $href = getHrefFromLinkWithText($linkText);

    clickLinkWithText($linkText);

    assertCurrentUrl(BASE_URL . "a/b/c/d/e/f/f.html");

    assertThat(strlen($href), is(0),
        sprintf("'%s' is not the shortest link possible", $href));
});

test('Shortest f index', function () {
    navigateTo(BASE_URL . 'a/b/c/d/e/f/f.html');

    $linkText = "shortest f/index.html";

    $href = getHrefFromLinkWithText($linkText);

    clickLinkWithText($linkText);

    assertCurrentUrl(BASE_URL . "a/b/c/d/e/f/");

    assertThat(strlen($href), is(1),
        sprintf("'%s' is not the shortest link possible", $href));
});

stf\runTests(getPointsReporter([1 => 1, 3 => 3, 6 => 6]));
